(function () {

	"use strict";

	//document.getElementById("form-message").insertAdjacentHTML('beforeend', '<div class="alert alert-success">OLASDASSD</div>');

	//===== Language Cookies
	function checkLanguageCookie() {
		const path = window.location.pathname;
		const langCookie = document.cookie.split(";").find((item) => item.trim().startsWith("lang="));

		if (!langCookie) {
			setLanguageCookie("pt");
		} else {
			const lang = langCookie.split("=")[1];

			if ((lang === "pt" && path !== "/") || (lang === "en" && path !== "/en/")) {
				switchLanguage(lang);
			}
		}
	}

	function setLanguageCookie(lang) {
		document.cookie = `lang=${lang}; path=/; SameSite=Lax; Secure`;
	}

	function switchLanguage(lang) {
		const newPath = (lang === "pt") ? "/" : "/en/";
		window.location.href = newPath;
	}

	const langElements = document.querySelectorAll(".lang-event")
	langElements.forEach(e => {
		e.addEventListener("click", evt => {
			let element = evt.target;
			if (element.tagName === "SPAN") {
				element = element.parentElement;
			}
			const lang = element.attributes["data-lang"].value;
			setLanguageCookie(lang);
			switchLanguage(lang);
		})
	})


	//===== Prealoder

	window.onload = function () {
		window.setTimeout(fadeout, 200);
		checkLanguageCookie();
	}

	function fadeout() {
		document.querySelector('.preloader').style.opacity = '0';
		document.querySelector('.preloader').style.display = 'none';
	}


	/*=====================================
	Sticky
	======================================= */
	window.onscroll = function () {
		var header_navbar = document.querySelector(".navbar-area");
		var sticky = header_navbar.offsetTop;

		if (window.pageYOffset > sticky) {
			header_navbar.classList.add("sticky");
		} else {
			header_navbar.classList.remove("sticky");
		}



		// show or hide the back-top-top button
		var backToTop = document.querySelector(".scroll-top");
		if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
			backToTop.style.display = "block";
		} else {
			backToTop.style.display = "none";
		}
	};

	//===== navbar-toggler
	let navbarToggler = document.querySelector(".navbar-toggler");
	navbarToggler.addEventListener('click', function () {
		navbarToggler.classList.toggle("active");
	})

	//WOW Scroll Spy
	var wow = new WOW({
		//disabled for mobile
		mobile: false
	});
	wow.init();

	//======= portfolio-btn active
	var elements = document.getElementsByClassName("portfolio-btn");
	for (var i = 0; i < elements.length; i++) {
		elements[i].onclick = function () {

			// remove class from sibling

			var el = elements[0];
			while (el) {
				if (el.tagName === "BUTTON") {
					//remove class
					el.classList.remove("active");

				}
				// pass to the new sibling
				el = el.nextSibling;
			}

			this.classList.add("active");
		};
	}

	var navLinks = document.querySelectorAll(".navbar-nav .nav-item a");

	navLinks.forEach(function (navLink) {
		navLink.addEventListener("click", function (event) {
			event.target.parentNode.childNodes.forEach(function (node) {
				if (!node.isEqualNode(event.target) && node.classList) {
					node.classList.toggle("show");
				}
			});
		});
	});

	navLinks.forEach(e =>
		e.addEventListener('click', () => {
			e.classList.toggle('show');
		})
	)

	//======= submit form
	function submitForm() {
		const formElement = document.getElementById('form-message');
		// Get form data
		const formData = new FormData(formElement);

		const data = []
		formData.forEach((formData, key) => {
			data.push({key: key, value: formData})
		});

		// Make a POST request using fetch
		fetch(formElement.action, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			},
			body: JSON.stringify(data),
		})
		.then(response => response.json())
		.then(data => console.log(data))
		.catch(error => console.log(error));
	}

	document.getElementById("form-message-submit")
		.addEventListener('click', function (event) {
			event.preventDefault();
			submitForm();
		});

})();